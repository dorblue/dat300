import numpy as np


class MyPerceptron:
    def __init__(self, n_epochs=20, l_rate=1.0, rand_state=1):
        self.n_epochs = n_epochs
        self.l_rate = l_rate
        self.rand_state = rand_state

    def train(self, pred_data, targ_data):
        feat_size = pred_data.shape[1]

        # Initializing the random number generator
        rand_gen = np.random.RandomState(self.rand_state)
        self.weights = rand_gen.normal(loc=0.0, scale=0.01, size=feat_size + 1)
        # self.weights[0] = -1
        self.errors = np.empty(self.n_epochs)

        for index in range(self.n_epochs):
            errors = 0
            for sample, target in zip(pred_data, targ_data):
                # Compute net output based on learning rule
                output = self.predict(sample)
                # Update net weights
                update = self.l_rate * (target - output)
                self.weights[1:] += sample * update
                self.weights[0] += update
                # Find out how successful was the classification (number of errors)
                errors += (output != target)
            self.errors[index] = errors
            index += 1
        return self

    def net_input(self, sample):
        # Compute net input based on predictor variables and weights
        return np.dot(sample, self.weights[1:]) + self.weights[0]

    def predict(self, sample):
        # Compute net output based on learning rule
        return np.where(self.net_input(sample) >= 0.0, 1, -1)
