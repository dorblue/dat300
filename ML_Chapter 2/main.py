import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from perceptron import MyPerceptron
from helpers import plot_decision_regions

# Importing iris data from .csv file
iris_data = pd.read_csv('iris.data', header=None)
iris_data.head()

# Choosing data for training
last_number = 100
sample_matrix = iris_data.iloc[0:last_number, [0, 3]].values
y = iris_data.iloc[0:last_number, 4].values
target_array = np.where(y == 'Iris-setosa', 1, -1)

# Plotting selected data
fst_number = target_array[target_array == 1].size
snd_number = target_array[target_array == -1].size

plt.scatter(sample_matrix[1: fst_number, 0],
            sample_matrix[1: fst_number, 1],
            color='red', marker='o', label='iris setosa')
plt.scatter(sample_matrix[fst_number: fst_number + snd_number, 0],
            sample_matrix[fst_number: fst_number + snd_number, 1],
            color='blue', marker='x', label='iris versicolor')
plt.xlabel('sepal length [cm]')
plt.ylabel('petal length [cm]')
plt.legend(loc='upper left')

# plt.savefig('ch2_30_08.png', dpi=600)
plt.show()

# Creating new object of class MyPerceptron
new_perc = MyPerceptron(l_rate=0.1, n_epochs=20)
# Training perceptron on selected data
new_perc.train(sample_matrix, target_array)

# Plotting classification errors
plt.plot(np.arange(1, new_perc.n_epochs + 1), new_perc.errors, marker='o')
plt.show()

# Performing classification on test data
sample_test = iris_data.iloc[0: last_number, [0, 3]].values
target_test = np.where(iris_data.iloc[0: last_number, 4].values == 'Iris-setosa', 1, -1)

# Plotting decision regions
plot_decision_regions(sample_test, target_test, classifier=new_perc)
plt.xlabel('sepal length [cm]')
plt.ylabel('petal length [cm]')
plt.legend(loc='upper left')

# plt.savefig('images/02_08.png', dpi=300)
plt.show()
